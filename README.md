
# pCT preprocessing software
## Authors: C.-A. collins-Fekete (UCL London), L. Volz (DKFZ Heidelberg)
Release Date: May 2021
Based on the preprocessing code from Robert P. Johnson (UCSC Santa Cruz), V. A. Bashkriov (LLU Loma Linda)
with additional contributions from J. Dickmann and G. Dedes (LMU Munich), P. Piersimoni (UiB, Bergen), V. Giacometti (University Wollongon)



## This README contains: 

1. How to execute the code and output

2. Config Variables

3. Hidden/obleque variables

4. Release notes

5. Problems/Ideas

## How to exectute the code and output

- The code depends on root 6.10.04, the 6.20 releases do not work, due to incompatible version upgrades. 

- To compile the program, a CMakeLists.txt and a shell script ./compile.sh is provided. Run ./compile.sh 

- The pCT_config.txt configuration file:

	- This file contains all user defined parameters for steering the preprocessing. 

	- Short descriptions of the variables are provided within the file, but 
	   are also explained below

- To exectute a calibration run: 

	- Enter the locations of calibration files you want to use in a txt file. Format:

		Dir/EmptyRun.dat\n
		Dir/CalibW_0b.dat\n
		Dir/CalibW_1b.dat\n
		
	   An example is provided with CalFileListHe.txt. 

	- In the pCT_config.txt file set the "calibrate" key word to 1. Set the name of your calib file
	   (usually pCTcalib.root). Rest of the parameters are the same as for preprocessing. 

	- Execute the command ./bin/main pCT_config.txt CalFileList.txt

- To execute a preprocessing run: 

	- First, you need to run a calibration run (you need a calibration file)

	- In the pCT_config.txt, you set the "calibrate" key to 0, you need to provide an output directory 

	- To execute the processing of a projection file ./bin/main pCT_config.txt Dir/FileName.dat 

- Output: 
	- The output will be a structured root file 

	- The relevant single event data is stored in the TTree "phase": The branches u/t/v are dim 4 arrays 
	   with the tracker measurements and positions, the variables x,y,z, px,py,pz are these positions and 
	   the resulting directions as used by Charles Unfolding code). E is a dim 5 array of detector energies, 
	   E_tot=sum(E). Wepl and theta selfexplanatory. ADC is the PMT ADC output in arbitrary units as used for 
	   debugging, time_stamp is the recorded hit time of the event. MaxEnergyTransFilter, ThresholdFilter, 
	   dEEFilter are flags for the respective filters acting on the energy deposi in each of the stages of 
	   the 5 stage detector. 

	- The TTree "header" contains relevant info for the projection: the projection angle, study_name,
	   code version etc., and importantly also info from the processing, like PMT gains and stage thresholds. 

	- The subfolder dEE holds the filtered dEE spectra for the run in preprocessing runs, or the full
	   spectra used to derive the dEE margins for a calibration run. 

	- The calWEPL folder contains the used calibration curves used to convert E-WEPL for this run

	- The Pedestal folder contains debugging histograms on the noise pedestals of the PMT output	

	- For calibration runs, there are additonal subfolders containing the calibration of the lateral light variation
	   and additional debuggig and consistency check data. These can be utilized using the QA scripts found in the
	   QA folder. 

	- If filesize becomes an issue (e.g. for 360 projections pCT data, where individual files are >100Mb in size)
	   the CToutput option in the pCT config can be set to 1, which will then only output the info to the phase
	   TTree necessary for CT recon. 


###################################################################################################
## Config Variables 

List of all variables: 
	
max_events = 0  # integer: hard cut on the maximum number of events to be used. Do not use with scanned beam<br />
dEEFilter = 1   # Integer/boolean: If dEE filter should be used or not<br />
dEEsig = 2.5    # Float: Validity region around primary dEE response (peak +- dEEsig*sigma)<br />
energy    = 200    # Beam Energy used in this experiment. Not relevant for analysis, will only be output to root file<br />
fraction  = 1      # Fraction of the input file to be read and processed (same function as max events, -> obsolete)<br />
time = 0           # Set nonzero to specify a maximum time stamp to analyze, in seconds (same function as max events or fraction-> obsolete)<br />
projection = -999. # Projection angle (taken from input data)<br />
continuous = 0     # Stepped or continuous scan? <br />
outputDir = ../Processed_Data   # Leave as '.' only if you want all the output to end up in the current directory<br />
calib  = pCTcalib.root # Calibration file<br />
log = NULL # Run Log file-> Only necessary for ccontinuous scan to identify phantom rotation at starting point<br />
angle = 0.        # For a continuous scan, if this angle is zero, then the program will try to get it from the log file<br />
thr0 = 1.0        # Thresholds on the minimum energy in a stage (MeV) to exclude noise (Not "Threshold Filter")<br />
thr1 = 1.0        # More fundamental: these are used to identify in which stage a particle stopped<br />
thr2 = 1.0        <br />
thr3 = 1.0	  <br />
thr4 = 1.0	  <br />
AutoThr = 0	# New (experimental) functionality to set the thresholds automatically from the noise pedestal distributions <br />
pedrng0 =  -500    # Lower limit of the ADC range where the noise floor is expected. <br />
pedrng1 =  -500	   # The code is looking for a peak within pedrng+2000ADC <br />
pedrng2 = -1000    <br />
pedrng3 = -1000    <br />
pedrng4 = -1000    <br />
partType = He     # Hydrogen or Helium ions <br />
calibrate = 1     # Set to 1 to analyze the calibration phantom run files to generate the calibration file <br />
CalibCurve = 0 # Use either regular multistage calibration (0, default), or single stage (from E_tot) with Energy vs Range (1), or Range Vs Energy (2), or hybrid method (3) (opt 1 at stage interfaces, opt 0 else) <br />
recalibrate = 1   # Integer/boolean: recalibrate PMT gains by using protons that miss the phantom. <br />
wedgeoffset = -50.8   # Offset of the calibration phantom from center , in the T direction, in mm (introduced for the HIT runs where it was -50.8) <br />
study = NULL      # Can be used to override automatic generation of the study name from the filename <br />
CTOutput = 0 # Flag to indicate whether the root file should comprise the full output or a reduced version with the necessary variables for CT imaging <br />
TpinOff1 = 0 # Tracker misalignment (relevant for 2019/2020 data, due to tracking deteector misalignment) <br />
TpinOff2 = 0 # The pin is the alignment point in the prepcessing code for the tracker boards (boards 1 to 4, with 1 proximal and 4 distal in beam dir) <br />
TpinOff3 = 0 # This enables to fine control alignment based on the empty run data (check the alignment plots from the calibration data and you can fine-tune alignment) <br />
TpinOff4 = 0 <br />
VpinOff1 = 0 <br />
VpinOff2 = 0 <br />
VpinOff3 = 0 <br />
VpinOff4 = 0 <br />



## Hidden/Oblique variables

- there are some hidden variables in the code, which are oblique to the user: 
	- In the Trackers, an event rejection is going on for events with multiple tracker hits,
	   as well as such where less than 7 tracker layers recorded a hit. In addition, there is 
	   an extrapolation for events that crossed the tracker gaps (visible in the tracksprofile
	   in pCTcalib.root). Currently, these events are omitted, due to additional ambiguity introduced
	   but they might also be re-included with the prior filter. 
	- In pCTgeo.cc/h there are a bunch of setup variables like detector geometry and setup geometry
	   Especially, there is a beam source parameter which is utilized by the tracking class to extrapolate
	   missing hits. 
	-  In the pCTcalib.cc/h codes there are parameters concerning the bin size of the calibration curves (in energy and WEPL)
	- In the pCTcalib.cc/h codes there are some parameters concerning which part of the calibration phantom is used for calibration (only wedge slopes currently)
	- In the pCTcalib.cc/h there are some variables concerning the extrapolation of the automatically set dEE filter to areas with sparse data or high noise in the dEE spectra
	- In the TVcorr.cc/h there is an "inbound" conditional, which removes particles which are expected to at some point have scattered out of the energy detector
	- In the pedGainCalib.cc/h there are parameters considering the determination of the PMT pedestal and gain peak values

## Release notes

- This is a test release, many areas need clean up and debugging
- Some parameters have become obsolete, but have not yet been removed


## Problems/Ideas

- Ring artifacts still present despite cleaned up code: current efforts to improve upon them
- Thresholds (usually 1MeV for each stage) are arbitrary -> a test for an automatic threshold finder was implemented
- Several ways of deriving the calibration curves can be performed. The standard (calibCurve=0) option is what has been used in all pCT collab publications














